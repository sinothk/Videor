package com.sinothk.videor.demo;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Toast;

import com.vincent.videocompressor.VideoCompress;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MainActivity extends AppCompatActivity {

    private final int VIDEO_REQUEST_CAMERA = 3; //录像

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        findViewById(R.id.button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                catchVideo();
            }
        });
    }

    //录像
    public void catchVideo() {
        Uri videoUriOri;
        try {
            videoFile = getVideoFile();
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
            videoUriOri = Uri.fromFile(videoFile);
        } else {
            videoUriOri = FileProvider.getUriForFile(this, this.getPackageName() + ".fileProvider", videoFile);
        }

        if (videoUriOri != null) {
            Intent takePictureIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, videoUriOri);
            takePictureIntent.putExtra(MediaStore.EXTRA_DURATION_LIMIT, 5);
            startActivityForResult(takePictureIntent, VIDEO_REQUEST_CAMERA);
        }
    }

    private File videoFile = null;

    private File getVideoFile() throws IOException {
        String imgNameOri = "VPic_" + new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());

        File pictureDirOri = new File(this.getExternalFilesDir(Environment.DIRECTORY_MOVIES).getAbsolutePath() + "/OriPicture");

        if (!pictureDirOri.exists()) {
            pictureDirOri.mkdirs();
        }

        File image = File.createTempFile(
                imgNameOri, /* prefix */
                ".mp4", /* suffix */
                pictureDirOri       /* directory */
        );

        return image;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK && requestCode == VIDEO_REQUEST_CAMERA) {
            doCompress(videoFile.getPath());
        }
    }

    private void doCompress(String path) {

        String imgNameOri = "VPic_" + new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());

        File pictureDirOri = new File(this.getExternalFilesDir(Environment.DIRECTORY_MOVIES).getAbsolutePath() + "/OriPicture");

        try {
            File compressFile = File.createTempFile(imgNameOri, ".mp4", pictureDirOri);

            VideoCompress.compressVideoLow(path, compressFile.getAbsolutePath(), new VideoCompress.CompressListener() {
                @Override
                public void onStart() {
                    System.out.println("------compress start------");
                }

                @Override
                public void onSuccess() {
                    System.out.println("------压缩前: " + new File(path).length());
                    System.out.println("------compress success------");
                    System.out.println("------compress success: " + compressFile.length());
                    System.out.println("------compress success: " + compressFile.getAbsolutePath());
                }

                @Override
                public void onFail() {
                    System.out.println("------compress fail------");
                    Toast.makeText(MainActivity.this, "文件压缩失败，请稍后重试", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onProgress(float percent) {
                    System.out.println("percent: " + percent);
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}